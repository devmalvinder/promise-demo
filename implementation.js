const PromiseLib = require("./PromiseLib");

const exampleOne = new PromiseLib(resolve => {
    setTimeout(() => resolve('Example One'), 100);
});

exampleOne.then(res => console.log('Ex One response: ', res));


const exampleTwo = new PLib(resolve => {
    setTimeout(() => reject('Example Two with Error'), 100);
});

exampleTwo.then(() => {}, err => console.error('Ex2: ERROR: ', err));

