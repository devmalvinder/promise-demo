const promiseTest = require("promises-aplus-tests");
const MyPromise = require("./PromiseLib");

promiseTest(MyPromise, function (err) {
    if(err) {
        console.error('Error Testing your Promise Library: ', err);
        return;
    }

    console.log('Tests run successfully');
});
