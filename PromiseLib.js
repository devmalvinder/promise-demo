export default class PromiseLib {
    constructor(callingMethod) {
        if (typeof callingMethod !== 'function') {
            throw new Error('Caller must be a Function');            
        }

        this.FULFILLED = 'FULFILLED';
        this.PENDING = 'PENDING';
        this.REJECTED = 'REJECTED';

        this.$state = this.PENDING;
        this.chainedMethods = [];

        const resolve = function(response) {
            if (this.$state !== this.PENDING) {
                return;
            }

            if (response != null && typeof response.then === 'function') {
                return response.then(resolve, reject);
              }

            this.$state = this.FULFILLED;
            this.$internalValue = response;

            for (const { onFulfilled } of chainedMethods) {
                onFulfilled(response);
            }
        };

        const reject = function(error) {
            if (this.$state !== this.PENDING) {
                return;
            }

            this.$state = this.REJECTED;
            this.$internalValue = error;

            for (const { onRejected } of this.chainedMethods) {
                onRejected(error);
            }
        };

        try {
            callingMethod(resolve, reject);
        }
        catch (error) {
            reject(error);
        }
    }

    then(onFulfilled, onRejected) {
        return new PromiseLib((resolve, reject) => {
            
            const onCompleted = function(response) {
                try {
                    resolve(onFulfilled(response));
                }
                catch (error) {
                    reject(error);
                }
            }

            const onError = function(error) {
                try {
                    reject(onRejected(error));
                }
                catch (err) {
                    reject(err);
                }
            }


            if (this.$state === this.FULFILLED) {
                onCompleted(this.$internalValue);
            }
            else if (this.$state === this.REJECTED) {
                onError(this.$internalValue);
            }
            else {
                this.chainedMethods.push({ onFulfilled: onCompleted, onRejected: onErr });
            }
        }); 
    }
}